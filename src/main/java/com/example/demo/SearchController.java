package com.example.demo;

import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/api/")
@RestController
public class SearchController {

    protected List<String> suggestions = Arrays.asList("JS", "JAVA", "JAVASCRIPT", "RXJAVA",
            "JANUARY", "JACKET", "JAR", "JUGGERNAUT");

    @GetMapping("search")
    protected @ResponseBody List<String> suggest(@RequestParam String text) {
        String searchText = text.toUpperCase();
        return suggestions.stream()
                .filter(o -> o.contains(searchText))
                .collect(Collectors.toList());
    }
}
