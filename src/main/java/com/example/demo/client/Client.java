package com.example.demo.client;

import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Arrays;

public class Client {

    public static void main(String[] args) {
        Flux<String> typings = Flux.create(emitter -> {
            try {
                emitter.next("j");
                Thread.sleep(10);
                emitter.next("ja");
                Thread.sleep(30);
                emitter.next("jav");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        WebClient webClient = WebClient.create("http://localhost:8080/api/search");
        typings
                .sample(Duration.ofMillis(50))
                .switchMap(
                        s -> webClient
                                .get()
                                .uri(uriBuilder -> uriBuilder.queryParam("text", s).build())
                                .retrieve()
                                .bodyToMono(String[].class)
                )
                .subscribe(value -> System.out.println(Arrays.toString(value)));

        typings.blockLast();
    }
}
